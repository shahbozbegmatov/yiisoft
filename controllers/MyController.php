<?php

namespace app\controllers;

use yii\web\Controller;

class MyController extends Controller
{
    public function actionIndex($fio = "Укажите ФИО")
    {
        return $fio;
    }

    public function actionAbout()
    {
        return $this->render('about1',[
            'firtsName' => "Margus"
        ]);
    }
}